import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './services/firebase.service';
import { FormControl, Validators } from '@angular/forms';
import { HasErrorState } from '@angular/material/core/common-behaviors/error-state';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
}) 
export class AppComponent implements OnInit {

  title = 'Itay Firebase login';
  isSignedIn = false;
  isLogin = false;
  constructor(public firebaseService: FirebaseService, private router: Router){}
    ngOnInit(){
      if (localStorage.getItem('user') !== null) // checking cookies if you are still logged in
      {
        this.isSignedIn = true
        this.router.navigate(['dashboard']); // fixing address
      }        
      else
      {
        this.isSignedIn = false
      }
      
  }
  async onSignup(email:string, password : string) {  // signup
    await this.firebaseService.signUp(email, password)
    if (this.firebaseService.isLoggedIn) // successful signup
      this.isSignedIn = true;    
      this.router.navigate(['dashboard']);
  }
  async onSignin(email: string, password: string) { // log in
    await this.firebaseService.signIn(email, password)
    if (this.firebaseService.isLoggedIn) // successful signup
      this.isSignedIn = true;
     this.router.navigate(['dashboard']);
  }
  onLogOut()  // log out
  {
    this.isSignedIn = false
    this.isLogin = true
  }

  hide = true;

  email = new FormControl('', [Validators.required, Validators.email]);  // validators for text fields
  pass = new FormControl('', [Validators.required, Validators.minLength(8)]);

  getErrorMessageMail() { // validating email adress
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Please enter a valid email address' : '';
  }

  getErrorMessagePass() { // validating password
    if (this.pass.hasError('required')) {
      return 'You must enter a value';
    }
    return this.pass.hasError('pass') ? '' : 'Please enter a valid password';
  }
  swapMode() {  // swapping between sign up and log in
    this.isLogin = !this.isLogin
  }
}
