import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  isLoggedIn = false;

  constructor(public firebaseAuth: AngularFireAuth, private router: Router) { }
  async signIn(email: string, password: string) {  // logging user to firebase
    await this.firebaseAuth.signInWithEmailAndPassword(email, password)
    .then (res=>{
      this.isLoggedIn = true
      localStorage.setItem('user', JSON.stringify(res.user))
    })
  }
  async signUp(email: string, password: string) {  // adding user to firebase
    await this.firebaseAuth.createUserWithEmailAndPassword(email, password)
      .then(res => {
        this.isLoggedIn = true
        localStorage.setItem('user', JSON.stringify(res.user))
      })
  }
  logOut() {  // logging user out of firebase
    this.firebaseAuth.signOut()
    localStorage.removeItem('user')
    this.router.navigate(['login'])
  }
}

/** @title Form field with error messages */
