import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import { HomeComponent } from './home/home.component';
import { FirebaseService } from './services/firebase.service';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ],
  imports: [
    RouterModule.forRoot([
      { path: 'signup', component: AppComponent },
      { path: 'login', component: AppComponent },
      { path: 'dashboard', component: HomeComponent },
      { path: '', redirectTo: 'signup', pathMatch: 'full' },
    ], {useHash: true}),
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyA4rT09qXJpS6MFoIUOBcswPHs6n6bmG9Q",
      authDomain: "yoozitay.firebaseapp.com",
      projectId: "yoozitay",
      storageBucket: "yoozitay.appspot.com",
      messagingSenderId: "776433300008",
      appId: "1:776433300008:web:3b2842d2d4cead19d1fb1d"
    }),
  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }

